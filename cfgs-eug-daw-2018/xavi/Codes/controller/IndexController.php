<?php
require_once(__DIR__.'/../model/Code.php');

class IndexController{
    
    public function generateList($num = 100){
        $list = array();
        for ( $i = 0 ; $i < $num ; $i++){
            $c = "AAAA";
            if($i >= 0 && $i < 10){
                $c = "A00".$i;
            }
            if($i >= 10 && $i < 100){
                $c = "A0".$i;
            }
            if($i >= 100 && $i < 1000){
                $c = "A".$i;
            }
            
            $element = $this->generateDetails($c);
            array_push($list, $element);
        }
        
        return $list;
    }
    
    public function generateDetails($code){
        $val = "";
        for( $i = 1 ; $i < strlen($code) ; $i++){
            $val .= $this->getBin($code[$i]);
            if($i+1 < strlen($code)){
                $val .= ".";
            }
        }
        return new Code($code, $val);
    }
    
    private function getBin($digit){
        switch($digit){
            case 0:
                return "0000";
                break;
            case 1:
                return "0001";
                break;
            case 2:
                return "0010";
                break;
            case 3:
                return "0011";
                break;
            case 4:
                return "0100";
                break;
            case 5:
                return "0101";
                break;
            case 6:
                return "0110";
                break;
            case 7:
                return "0111";
                break;
            case 8:
                return "1000";
                break;
            case 9:
                return "1001";
                break;
            default:
                return "XXXX";
                break;
        };
    }
    
    public function saveCode($paramcode, $paramvalue){
        $this->checkCode($paramcode);
        $this->checkValue($paramvalue);
        
        ...
        
        $c = new Code($paramcode, $paramvalue);
        
        ...
        
    }
    
    private function checkCode($c){
        
    }
    
    private function checkValue($v){
        
    }
}